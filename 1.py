def calc_fuel(m):
    return int(m/3)-2

with open('input1') as input:
    print('p1:',sum([calc_fuel(int(l)) for l in input.readlines()]))

with open('input1') as input:
    fuel_total = 0
    for l in input.readlines():
        mass = int(l)
        last_fuel = calc_fuel(mass)
        fuel_sum = 0
        while last_fuel > 0:
            fuel_sum += last_fuel
            last_fuel = calc_fuel(last_fuel)
        fuel_total += fuel_sum
print('p2:',fuel_total)
