import intcode
with open('input9') as input:
    raw = input.read()
code = intcode.from_str(raw)
c = intcode.Computer(code)

# Part 1
c.run([1])

# Part 2
c.run([2])
