import intcode
with open('input5') as input:
    raw = input.read()
c = intcode.Computer(intcode.from_str(raw))

# Part 1
c.run([1])

# Part 2
c.run([5])
