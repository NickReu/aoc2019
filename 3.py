with open('input3') as input:
    lines = input.readlines()

# Part 1
wires = [set(),set()]
minmatch = float('inf')
for i, wire in enumerate(lines):
    goes = wire.split(',')
    x = 0
    y = 0
    for gi, go in enumerate(goes):
        direction = go[0]
        distance = int(go[1:])
        if direction == 'R':
            for _ in range(distance):
                x += 1
                wires[i].add((x,y))
                if i == 1 and abs(x)+abs(y) < minmatch and (x,y) in wires[0]:
                    minmatch = abs(x)+abs(y)
        elif direction == 'L':
            for _ in range(distance):
                x -= 1
                wires[i].add((x,y))
                if i == 1 and abs(x)+abs(y) < minmatch and (x,y) in wires[0]:
                    minmatch = abs(x)+abs(y)
        elif direction == 'U':
            for _ in range(distance):
                y += 1
                wires[i].add((x,y))
                if i == 1 and abs(x)+abs(y) < minmatch and (x,y) in wires[0]:
                    minmatch = abs(x)+abs(y)
        elif direction == 'D':
            for _ in range(distance):
                y -= 1
                wires[i].add((x,y))
                if i == 1 and abs(x)+abs(y) < minmatch and (x,y) in wires[0]:
                    minmatch = abs(x)+abs(y)
print('p1:', minmatch)

# Part 2
wires = [set(),set()]
d = {}
minmatch = float('inf')
for i, wire in enumerate(lines):
    goes = wire.split(',')
    x = 0
    y = 0
    steps = 0
    for gi, go in enumerate(goes):
        direction = go[0]
        distance = int(go[1:])
        if direction == 'R':
            for _ in range(distance):
                x += 1
                steps += 1
                if i == 0:
                    d[str(x) + ';' + str(y)] = steps
                wires[i].add((x,y))
                try:
                    if i == 1 and steps + d[str(x) + ';' + str(y)] < minmatch and (x,y) in wires[0]:
                        minmatch = steps + d[str(x) + ';' + str(y)]
                except:
                    pass
        elif direction == 'L':
            for _ in range(distance):
                x -= 1
                steps += 1
                if i == 0:
                    d[str(x) + ';' + str(y)] = steps

                wires[i].add((x,y))
                try:
                    if i == 1 and steps + d[str(x) + ';' + str(y)] < minmatch and (x,y) in wires[0]:
                        minmatch = steps + d[str(x) + ';' + str(y)]
                except:
                    pass
        elif direction == 'U':
            for _ in range(distance):
                y += 1
                steps += 1
                if i == 0:
                    d[str(x) + ';' + str(y)] = steps
                wires[i].add((x,y))
                try:
                    if i == 1 and steps + d[str(x) + ';' + str(y)] < minmatch and (x,y) in wires[0]:
                        minmatch = steps + d[str(x) + ';' + str(y)]
                except:
                    pass
        elif direction == 'D':
            for _ in range(distance):
                y -= 1
                steps += 1
                if i == 0:
                    d[str(x) + ';' + str(y)] = steps
                wires[i].add((x,y))
                try:
                    if i == 1 and steps + d[str(x) + ';' + str(y)] < minmatch and (x,y) in wires[0]:
                        minmatch = steps + d[str(x) + ';' + str(y)]
                except:
                    pass
print('p2:',minmatch)
