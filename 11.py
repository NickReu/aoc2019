import intcode
import numpy as np
from collections import defaultdict
import matplotlib.pyplot as plt
with open('input11') as input:
    raw = input.read()
code = intcode.from_str(raw)
rbrain = intcode.Computer(code, print = False)

# Part 1
painted = set()
panels = defaultdict(lambda: 0)
coords = [0,0]
facing = 0
rbrain.run([panels[tuple(coords)]],halt=True)
while rbrain.state == 'paused':
    paint = rbrain.outputs[-2]
    panels[tuple(coords)] = paint
    painted.add(tuple(coords))
    dir = rbrain.outputs[-1]
    facing = (facing + 1 if dir else facing - 1) % 4
    if facing == 0:
        coords[1] += 1
    elif facing == 1:
        coords[0] += 1
    elif facing == 2:
        coords[1] -= 1
    elif facing == 3:
        coords[0] -= 1
    rbrain.give_input(panels[tuple(coords)])
print('p1:',len(painted))

# Part 2
painted = set()
panels = defaultdict(lambda: 0)
panels[(0,0)] = 1
coords = [0,0]
facing = 0
rbrain.run([panels[tuple(coords)]],halt=True)
while rbrain.state == 'paused':
    paint = rbrain.outputs[-2]
    panels[tuple(coords)] = paint
    painted.add(tuple(coords))
    dir = rbrain.outputs[-1]
    facing = (facing + 1 if dir else facing - 1) % 4
    if facing == 0:
        coords[1] += 1
    elif facing == 1:
        coords[0] += 1
    elif facing == 2:
        coords[1] -= 1
    elif facing == 3:
        coords[0] -= 1
    rbrain.give_input(panels[tuple(coords)])
image = np.zeros((100,100))
for k in panels:
    image[k[0]+50,k[1]+50] = panels[k]
plt.imshow(image)
plt.show()
