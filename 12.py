from itertools import combinations
import numpy as np
with open('input12') as input:
    raw = input.read()
lines = [l for l in raw.split('\n') if l]
moons = [{'p':{},'v':{'x':0,'y':0,'z':0}} for _ in range(4)]
dims = ['x','y','z']
for li, line in enumerate(lines):
    x, y, z = [int(c.strip(' xyz=<>')) for c in line.split(',')]
    moons[li]['p']['x'] = x
    moons[li]['p']['y'] = y
    moons[li]['p']['z'] = z
initial_moons = moons.copy()

# Part 1
time = 0
for time in range(1000):
    for m1, m2 in combinations(moons, r=2):
        for d in dims:
            if m1['p'][d] < m2['p'][d]:
                m1['v'][d] += 1
                m2['v'][d] -= 1
            elif m1['p'][d] > m2['p'][d]:
                m1['v'][d] -= 1
                m2['v'][d] += 1
    for m in moons:
        for d in dims:
            m['p'][d] += m['v'][d]
total_energy = 0
energy = {}
for m in moons:
    for type in ('p','v'):
        energy[type] = 0
        for d in dims:
            energy[type] += abs(m[type][d])
    total_energy += energy['v'] * energy['p']
print('p1:',total_energy)

# Part 2
period = {}
for d in dims:
    time = 0
    moons = initial_moons.copy()
    first = tuple((tuple(m['p'][d] for m in moons),tuple(m['v'][d] for m in moons)))
    while time == 0 or state != first:
        time += 1
        for m1, m2 in combinations(moons, r=2):
            if m1['p'][d] < m2['p'][d]:
                m1['v'][d] += 1
                m2['v'][d] -= 1
            elif m1['p'][d] > m2['p'][d]:
                m1['v'][d] -= 1
                m2['v'][d] += 1
        for m in moons:
            m['p'][d] += m['v'][d]
        state = tuple((tuple(m['p'][d] for m in moons),tuple(m['v'][d] for m in moons)))
    period[d] = time
ps = [period[d] for d in dims]
print('p2:',np.lcm.reduce(ps))
