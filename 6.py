with open('input6') as input:
    raw = input.read()

lines = raw.split('\n')

orbits = {}
for line in lines:
    if not line == '':
        l, r = line.split(')')
        orbits[r] = l

# Part 1
count = 0
def co(obj):
    global count
    if obj in orbits:
        count += 1
    if obj in orbits and orbits[obj] in orbits:
        co(orbits[obj])
for obj in orbits:
    co(obj)
print('p1:',count)

# Part 2
whereme = orbits['YOU']
go = orbits['SAN']
t1 = whereme
t2 = go
c1 = 0
c2 = 0
while t1 != t2:
    t2 = go
    c2 = 0
    while t1 != t2:
        if t2 in orbits:
            t2 = orbits[t2]
            c2 += 1
        else:
            break
    if t1 == t2:
        break
    if t1 in orbits:
        t1 = orbits[t1]
        c1 += 1
print('p2:',c1+c2)
