from collections import defaultdict

class Computer():
    def __init__(self, memory, id = 0, print = True):
        self.base_mem = memory.copy()
        self.set_memory(self.base_mem)
        self.pc = 0
        self.inputs = []
        self.prefix = None
        self.outputs = []
        self.id = id
        self.rb = 0
        self.state = 'stopped'
        self.print = print

    def set_memory(self, mem):
        cp_mem = mem.copy()
        self.mem = defaultdict(int)
        for i, z in enumerate(mem):
            self.mem[i] = z

    def prep_instruction(self, modes, num_params):
        self.fill_modes(modes, num_params + 1)
        return (self.look(self.pc+i+1,modes[i]) for i in range(num_params))

    def do_op(self, f, modes, num_params):
        looks = self.prep_instruction(modes, num_params)
        self.mem[self.look_addr(self.pc + num_params + 1, modes[-1])] = f(*looks)
        self.pc += num_params + 2

    def jump_cond(self, f, modes, num_params):
        looks = self.prep_instruction(modes, num_params)
        if f(*looks):
            self.pc = self.look(self.pc + num_params + 1, modes[-1])
        else:
            self.pc += num_params + 2

    def run_noun_verb(self, n, v):
        self.mem = self.base_mem.copy()
        self.mem[1] = n
        self.mem[2] = v
        return self.run(reset=False)

    def get_input(self):
        if self.inputs:
            return self.inputs.pop(0)
        else:
            raise RuntimeError('Not enough inputs provided')

    def give_input(self, inp):
        return self.run([inp], False, True, self.pc)

    def look(self, p, mode):
        return self.mem[self.look_addr(p, mode)]

    def look_addr(self, p, mode):
        addr = None
        if mode == 0:
            addr = self.mem[p]
        elif mode == 1:
            addr = p
        elif mode == 2:
            addr = self.rb + self.mem[p]
        else:
            errcode = f'Invalid Intcode pmode ({mode}) at {p}'
            raise ValueError(errcode)
        return addr

    def extract_modes(self, opcode):
        op = opcode % 100
        mode_int = opcode // 100
        modes = []
        while mode_int:
            modes.append(mode_int % 10)
            mode_int = mode_int // 10
        return op, modes

    def fill_modes(self, modes, num_params):
        while num_params > len(modes):
            modes.append(0)

    def run(self, inputs = [], reset = True, halt = False, pc = 0):
        self.pc = pc
        self.state = 'running'
        if reset:
            self.set_memory(self.base_mem)
            self.outputs = []
            self.rb = 0
        if inputs:
            self.inputs = inputs
        while 0 <= self.pc and self.pc < len(self.mem):
            op, modes = self.extract_modes(self.mem[self.pc])
            if op == 1:
                self.do_op(lambda x, y: x+y, modes, 2)
            elif op == 2:
                self.do_op(lambda x, y: x*y, modes, 2)
            elif op == 3:
                if len(inputs) or not halt:
                    self.do_op(self.get_input, modes, 0)
                elif halt:
                    self.state = 'paused'
                    return 'waiting for input'
                else:
                    raise RuntimeError('Not enough inputs provided')
            elif op == 4:
                self.fill_modes(modes,1)
                self.outputs.append(self.look(self.pc+1,modes[0]))
                if self.print:
                    if not self.prefix:
                        self.prefix = f'({self.id}-out)'
                    else:
                        self.prefix = f'  ({self.id}) *'
                    print(self.prefix,self.look(self.pc+1,modes[0]))
                self.pc += 2
            elif op == 5:
                self.jump_cond(lambda x: x, modes, 1)
            elif op == 6:
                self.jump_cond(lambda x: not x, modes, 1)
            elif op == 7:
                self.do_op(lambda x,y: 1 if x < y else 0, modes, 2)
            elif op == 8:
                self.do_op(lambda x,y: 1 if x == y else 0, modes, 2)
            elif op == 9:
                self.fill_modes(modes,1)
                self.rb += self.look(self.pc + 1, modes[0])
                self.pc += 2
            elif op == 99:
                self.state = 'stopped'
                return self.mem[0]
            else:
                errcode = f'Invalid Intcode op ({op}) at {self.pc}'
                raise ValueError(errcode)
        raise IndexError('Intcode pc outside of memory')

def from_str(s):
    return [int(c) for c in s.split(',') if not c == '']
