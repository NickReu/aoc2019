import numpy as np
from math import gcd
from matplotlib.pyplot import imshow, show
with open('input10') as input:
    raw = input.read()

field = np.transpose([list(l) for l in raw.split('\n') if l != ''])
x_len, y_len = field.shape

def reduce(i, j):
    ijgcd = gcd(i,j)
    if ijgcd < 0:
        ijgcd *= -1
    return i // ijgcd, j // ijgcd, ijgcd

def can_detect(x, y, i, j):
    diff_i = x - i
    diff_j = y - j
    if diff_i and diff_j:
        ri, rj, ijgcd = reduce(diff_i,diff_j)
        mag = (diff_i // ri)
        mag -= 1
        while (mag > 0):
            check_x = x - mag * ri
            check_y = y - mag * rj
            if field[check_x, check_y] == '#':
                return False
            mag -= 1
    elif diff_i > 0:
        mag = diff_i - 1
        while (mag > 0):
            check_x = x - mag
            check_y = y
            if field[check_x, check_y] == '#':
                return False
            mag -= 1
    elif diff_j > 0:
        mag = diff_j - 1
        while (mag > 0):
            check_x = x
            check_y = y - mag
            if field[check_x, check_y] == '#':
                return False
            mag -= 1
    elif diff_i < 0:
        mag = diff_i + 1
        while (mag < 0):
            check_x = x - mag
            check_y = y
            if field[check_x, check_y] == '#':
                return False
            mag += 1
    elif diff_j < 0:
        mag = diff_j + 1
        while (mag < 0):
            check_x = x
            check_y = y - mag
            if field[check_x, check_y] == '#':
                return False
            mag += 1
    return True

def calc_detect(x,y):
    count = 0
    for i in range(x_len):
        for j in range(y_len):
            if x == i and y == j:
                continue
            if not field[i,j] == '#':
                continue
            check = can_detect(x,y,i,j)
            if check:
                count += 1
    return count

def angle(x,y,i,j):
    ret = np.arctan2(x-i,-y+j)
    if ret == np.pi:
        ret = -np.pi
    return ret

def print_order(a):
    a = np.flip(a,1)
    out = np.zeros(field.shape)
    for k in range(len(a)):
        out[a[k][0],a[k][1]] = k + 30
    imshow(out, cmap='hot')
    show()

# Part 1
max = 0
max_i = None
for j in range(y_len):
    for i  in range(x_len):
        if not field[i,j] == '#':
            continue
        detectable = calc_detect(i,j)
        if detectable > max:
            max = detectable
            max_i = (i,j)
print('p1:',max)

# Part 2
x, y = max_i
coords = [(i, j) for j in range(y_len) for i in range(x_len)]
coords_sort = sorted(coords, key = lambda u: angle(x,y,*u))
destroyed_count = 0
destroyed = []
stop = False
while destroyed_count < 200:
    for i, j in coords_sort:
        if not field[i,j] == '#':
            continue
        if i == x and y == j:
            continue
        if can_detect(x,y,i,j):
            destroyed.append((i,j))
            destroyed_count += 1
    for i, j in destroyed:
        field[i,j] = '.'
print('p2:',destroyed[199][0]*100+destroyed[199][1])
