import intcode
import itertools
with open('input7') as input:
    raw = input.read()
code = intcode.from_str(raw)

# Part 1
max = 0
amps = [intcode.Computer(code) for _ in range(5)]
for i, j, k, l, m in itertools.permutations(range(5)):
    amps[0].run([i,0])
    amps[1].run([j,amps[0].outputs[0]])
    amps[2].run([k,amps[1].outputs[0]])
    amps[3].run([l,amps[2].outputs[0]])
    amps[4].run([m,amps[3].outputs[0]])
    if amps[4].outputs[0] > max:
        max = amps[4].outputs[0]
print('p1:',max)

# Part 2
max = 0
amps = [intcode.Computer(code, id = id) for id in range(5)]
for i, j, k, l, m in itertools.permutations(range(5,10)):
    amps[0].run([i,0], halt = True)
    amps[1].run([j,amps[0].outputs[-1]], halt = True)
    amps[2].run([k,amps[1].outputs[-1]], halt = True)
    amps[3].run([l,amps[2].outputs[-1]], halt = True)
    done = amps[4].run([m,amps[3].outputs[-1]], halt = True)
    while type(done) == str:
        amps[0].give_input(amps[4].outputs[-1])
        amps[1].give_input(amps[0].outputs[-1])
        amps[2].give_input(amps[1].outputs[-1])
        amps[3].give_input(amps[2].outputs[-1])
        done = amps[4].give_input(amps[3].outputs[-1])
    if amps[4].outputs[-1] > max:
        max = amps[4].outputs[-1]

print('p2:',max)
