import numpy as np
size = (25, 6)
with open('input8') as input:
    raw = input.read()

def index(layer, x, y):
    return layer*size[0]*size[1] + y * size[0] + x

# Part 1
layer = 0
count_0s = {}
count_1s = {}
count_2s = {}
x = 0
y = 0
while index(layer, x, y) < len(raw):
    x = 0
    y = 0
    for x in range(size[0]):
        for y in range(size[1]):
            if raw[index(layer, x, y)] == '0':
                if layer in count_0s:
                    count_0s[layer] += 1
                else:
                    count_0s[layer] = 1
            if raw[index(layer, x, y)] == '1':
                if layer in count_1s:
                    count_1s[layer] += 1
                else:
                    count_1s[layer] = 1
            if raw[index(layer, x, y)] == '2':
                if layer in count_2s:
                    count_2s[layer] += 1
                else:
                    count_2s[layer] = 1
            y += 1
        x += 1
    layer += 1
minlayer = min(range(len(count_0s)), key = lambda x: count_0s[x])

print('p1:',count_1s[minlayer]*count_2s[minlayer])

# Part 2
img = np.zeros(size)
for x in range(size[0]):
    for y in range(size[1]):
        layer = 0
        while raw[index(layer, x, y)] == '2':
            layer += 1
        img[x,y] = raw[index(layer, x, y)]
        y += 1
    x += 1
print(np.matrix(img))
