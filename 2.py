import intcode
with open('input2') as input:
    raw = input.read()
codes = intcode.from_str(raw)
c = intcode.Computer(codes)

# Part 1
print('p1:',c.run_noun_verb(12,2))

# Part 2
for noun in range(0,100):
    for verb in range(0,100):
        if c.run_noun_verb(noun,verb) == 19690720:
            result2 = 100*noun+verb
            break
print('p2:',result2)
