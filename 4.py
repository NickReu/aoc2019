input = (254032,789860)

# Part 1
count = 0
for i in range(*input):
    doubles = False
    counts = True
    lastc = 0
    for c in str(i):
        if int(c) < lastc:
            counts = False
            break
        elif int(c) == lastc:
            doubles = True
        lastc = int(c)
    if counts and doubles:
        count += 1

print('p1:',count)

# Part 2
count = 0
for i in range(*input):
    doubles = False
    counts = True
    lastc = 0
    for c in str(i):
        if int(c) < lastc:
            counts = False
            break
        if not doubles and str(i).count(c) == 2:
            doubles = True
        lastc = int(c)
    if counts and doubles:
        count += 1
print('p2:',count)
